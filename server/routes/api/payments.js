const Razorpay = require('razorpay');
const stripe = require('stripe')('sk_test_yKYO9voP5MLaiqQN9LOParTX');
const axios = require('axios');


const razorpay = new Razorpay({
    key_id: 'rzp_test_pEBJwqm1DVxFh1',
    key_secret: 'v9U0lSts1WAlJSWJq1ZLOLFz'
});

module.exports = (router) => {
    router.post('/payments', function (req, res) {
        let payment_id = req.body.payment_id;
        razorpay.payments.fetch(payment_id)
            .then((response) => {
                res.json(response);
            }).catch((error) => {
                res.json(error);
            })
    });
    router.post("/charge", (req, res) => {
        stripe.customers.create({
            email: req.body.stripeEmail,
            source: req.body.stripeToken
        })
            .then(customer =>
                stripe.charges.create({
                    amount: req.body.amount,
                    description: req.body.description,
                    currency: req.body.currency,
                    customer: customer.id
                })
                    .then(function (charge) {
                        res.json(charge);
                    }).catch(function (err) {
                        res.json(err);
                    })
            );
    });
}
