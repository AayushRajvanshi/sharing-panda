module.exports = (router, passport) => {
    router.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/fundraisers',
        failureRedirect: '/signup',
        failureFlash: true
    }));
    router.post('/login', passport.authenticate('local-login', {
        successRedirect: '/fundraisers',
        failureRedirect: '/login',
        failureFlash: true
    }));

    router.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));
    router.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/fundraisers',
            failureRedirect: '/login'
        })
    );

    router.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));
    router.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect: '/fundraisers',
            failureRedirect: '/login'
        })
    );

    router.get('/auth/twitter', passport.authenticate('twitter'));
    router.get('/auth/twitter/callback',
        passport.authenticate('twitter', {
            successRedirect: '/fundraisers',
            failureRedirect: '/login'
        })
    );
}

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}