import React, { Component } from 'react';
import * as Redux from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';

export class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleLogin = (e) => {
        e.preventDefault();
        let data = this.state;
        axios({
            method: 'post',
            url: '/api/v1/login',
            data: data
        })
            .then((user) => {
                this.resetForm();
            });
    }

    resetForm = () => {
        this.setState({
            email: '',
            password: '',
        })
    }

    onFacebookLogin = (e) => {
        e.preventDefault();
        axios({
            method: 'get',
            url: '/api/v1/auth/facebook'
        })
            .then((user) => {
                console.log(user);
            });
    }

    onGoogleLogin = (e) => {
        e.preventDefault();
        axios({
            method: 'get',
            url: '/api/v1/auth/google'
        })
            .then((user) => {
                console.log(user);
            });
    }

    onTwitterLogin = (e) => {
        e.preventDefault();
        axios({
            method: 'get',
            url: '/api/v1/auth/twitter'
        })
            .then((user) => {
                console.log(user);
            });
    }

    render() {
        return (
            <form id="login-form" className="uk-form-stacked" onSubmit={this.handleSubmit}>
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <div className="uk-inline">
                            <span className="uk-form-icon" data-uk-icon="icon: user"></span>
                            <input id="email" name="email" className="uk-input uk-form-width-large" id="form-stacked-text" type="text" placeholder="Email Id" onChange={this.handleInputChange} />
                        </div>
                    </div>
                </div>
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <div className="uk-inline">
                            <span className="uk-form-icon" data-uk-icon="icon: lock"></span>
                            <input id="password" name="password" className="uk-input uk-form-width-large" id="form-stacked-text" type="password" placeholder="Password" onChange={this.handleInputChange} />
                        </div>
                    </div>
                </div>
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <button className="uk-button uk-button-large uk-button-primary uk-width-1-1" type="button" onClick={this.handleLogin}>SIGN IN</button>
                    </div>
                </div>
                <div className="uk-clearfix">
                    <div className="uk-float-left">
                        <label style={{ 'fontSize': '0.7rem' }}>Forgot Details?</label>
                    </div>
                    <div className="uk-float-right">
                        <label style={{ 'fontSize': '0.7rem' }}><input className="uk-checkbox" type="checkbox" /> Remember me</label>
                    </div>
                </div>
                <hr className="uk-divider-icon" />
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <a href="/api/v1/auth/google" target="_self">
                            <button id="google-button" type="button" className="uk-visible@s uk-button uk-button-large uk-button-primary"><span className="fa fa-google-plus"></span>Google</button>
                        </a>
                        <a href="/api/v1/auth/facebook" target="_self">
                            <button id="facebook-button" type="button" className="uk-visible@s uk-button uk-button-large uk-button-primary"><span className="fa fa-facebook"></span>Facebook</button>
                        </a>
                        <a href="/api/v1/auth/twitter" target="_self">
                            <button id="twitter-button" type="button" className="uk-visible@s uk-button uk-button-large uk-button-primary"><span className="fa fa-twitter"></span>Twitter</button>
                        </a>
                    </div>
                </div>
                <div className="uk-margin-small">
                    <p style={{ 'fontSize': '0.7rem' }}>I don't have an account yet. <Link to="/signup"><span className="uk-link" style={{ 'fontSize': '0.7rem' }}>Sign Up</span></Link> </p>
                </div>
            </form>
        );
    }
}

export default LoginForm;