import React, { Component } from 'react';
import * as Redux from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';

export class SignUpForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            password: ''
        }
    }
    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let data = this.state;
        axios({
            method: 'post',
            url: '/api/v1/signup',
            data: data
        })
            .then((user) => {
                this.resetForm();
            });
    }

    resetForm = () => {
        this.setState({
            first_name: '',
            last_name: '',
            email: '',
            password: '',
        })
    }

    render() {
        return (
            <form id="signup-form" className="uk-form-stacked" onSubmit={this.handleSubmit}>
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <div className="uk-inline">
                            <span className="uk-form-icon" data-uk-icon="icon: user"></span>
                            <input name="first_name" id="first_name" value={this.state.first_name} className="uk-input uk-form-width-large" type="text" placeholder="First Name" onChange={this.handleInputChange} />
                        </div>
                    </div>
                </div>
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <div className="uk-inline">
                            <span className="uk-form-icon" data-uk-icon="icon: user"></span>
                            <input name="last_name" id="last_name" value={this.state.last_name} className="uk-input uk-form-width-large" type="text" placeholder="Last Name" onChange={this.handleInputChange} />
                        </div>
                    </div>
                </div>
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <div className="uk-inline">
                            <span className="uk-form-icon" data-uk-icon="icon: user"></span>
                            <input name="email" id="email" value={this.state.email} className="uk-input uk-form-width-large" id="form-stacked-text" type="text" placeholder="Email Id" onChange={this.handleInputChange} />
                        </div>
                    </div>
                </div>
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <div className="uk-inline">
                            <span className="uk-form-icon" data-uk-icon="icon: lock"></span>
                            <input name="password" id="password" value={this.state.password} className="uk-input uk-form-width-large" id="form-stacked-text" type="password" placeholder="Password" onChange={this.handleInputChange}  />
                        </div>
                    </div>
                </div>
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <button className="uk-button uk-button-large uk-button-primary uk-width-1-1 ">SIGN UP</button>
                    </div>
                </div>
                <hr className="uk-divider-icon" />
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <button id="google-button" className="uk-button uk-button-large uk-button-primary uk-width-1-1"><span className="uk-margin-small-right" style={{ 'color': 'white' }} data-uk-icon="icon: google"></span>Google</button>
                    </div>
                </div>
                <div className="uk-margin-small">
                    <div className="uk-form-controls">
                        <button id="facebook-button" className="uk-button uk-button-large uk-button-primary uk-width-1-1"> <span className="uk-margin-small-right" style={{ 'color': 'white' }} data-uk-icon="icon: facebook"></span>Facebook</button>
                    </div>
                </div>
                <p style={{ 'fontSize': '0.7rem', 'marginBottom': '0px' }}>Already have an account? <Link to="/login"><span className="uk-link" style={{ 'fontSize': '0.7rem', 'marginBottom': '0px' }} data-uk-toggle="target: #login-modal">Sign In</span></Link></p>
            </form>
        );
    }
}

export default SignUpForm;