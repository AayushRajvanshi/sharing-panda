import React from 'react'
import { NavLink, withRouter } from 'react-router-dom';
import img1 from '../../../public/assets/images/img1.jpg';
import logo from '../../../public/assets/images/sharing-panda-logo.svg';

const PrimaryHeader = ({ match }) => (
    <nav className="uk-navbar-container uk-navbar-default" style={{ 'background': '#F2D023' }} data-uk-navbar>
        <div className="uk-navbar-center">
            <div className="uk-navbar-center-left">
                <ul className="uk-navbar-nav">
                    <li style={{ 'color': 'black' }}>
                        <NavLink style={{ 'color': 'black' }} to='/fundraisers/start-fundraiser' activeClassName="uk-active">Start Your Fundraiser</NavLink>
                    </li>
                    <li>
                        <NavLink style={{ 'color': 'black' }} to='/fundraisers' activeClassName="uk-active">Browse Fundraisers</NavLink>
                    </li>
                </ul>
            </div>
            <NavLink className="uk-navbar-item uk-logo" style={{ 'color': 'black' }} to='/' exact activeClassName="uk-active">
                <img src={logo} width="180" height="80" alt="" data-uk-svg />
            </NavLink>
            <div className="uk-navbar-center-right">
                <ul className="uk-navbar-nav">
                    <li style={{ 'color': 'black' }}>
                        <NavLink style={{ 'color': 'black' }} to='/petitions' activeClassName="uk-active">Browse Petitions</NavLink>
                    </li>
                    <li style={{ 'color': 'black' }}>
                        <NavLink style={{ 'color': 'black' }} to='/petitions/start-petition' activeClassName="uk-active">Start Your Petition</NavLink>
                    </li>
                </ul>
            </div>
        </div>
        <div className="uk-navbar-right uk-margin-right">
            <ul className="uk-navbar-nav">
                <li className="uk-active">
                    <NavLink style={{ 'color': 'black' }} to='/login' activeClassName="uk-active"><i className="fa fa-search" aria-hidden="true"></i></NavLink>
                </li>
                <li className="uk-active">
                    <NavLink style={{ 'color': 'black' }} to='/login' activeClassName="uk-active"><i className="fa fa-bell-o" aria-hidden="true"></i></NavLink>
                </li>
                <li className="uk-active">
                    <NavLink style={{ 'color': 'black' }} to='/login' activeClassName="uk-active"><i className="fa fa-user-circle-o" aria-hidden="true"></i></NavLink>
                </li>
            </ul>
        </div>
    </nav >
)

export default withRouter(PrimaryHeader);