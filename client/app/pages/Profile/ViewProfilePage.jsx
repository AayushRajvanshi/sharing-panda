import React, { Component } from 'react';

class ViewProfilePage extends Component {
    render() {
        let {userId} = this.props.match.params;
        return (
            <div>
                {`ViewProfilePage for user ${userId}`}
            </div>
        );
    }
}

export default ViewProfilePage;