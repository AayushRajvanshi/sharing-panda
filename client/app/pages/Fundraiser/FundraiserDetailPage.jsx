import React, { Component } from 'react';;
import { NavLink } from 'react-router-dom'
import CommentsContainer from '../../components/Comments/CommentsContainer';
import DonationContainer from '../../components/Donation/DonationContainer';
import img1 from '../../../public/assets/images/img1.jpg';
import axios from 'axios';
import Quill from 'quill';

class FundraiserDetailPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fundraiser: {}
        }
    }

    componentDidMount() {
        let quill = new Quill('#story', {
            theme: 'bubble'
        });
        quill.enable(false);

        axios({
            method: 'get',
            url: `/api/v1/fundraisers/${this.props.match.params.fundraiserId}`
        })
            .then((fundraiser) => {
                this.setState({
                    fundraiser: fundraiser.data
                }, () => {
                    quill.setContents(JSON.parse(this.state.fundraiser.story));
                })
            });
    }

    render() {
        let { fundraiser } = this.state;
        return (
            <div className="uk-container uk-container-small">
                <div className="uk-section">
                    <h2 className="uk-margin-remove-bottom">{fundraiser.title}</h2>
                    <p>{fundraiser.description}</p>
                    <div className="uk-background-cover uk-height-large" style={{ 'backgroundImage': 'url(' + img1 + ')' }}>
                    </div>
                    <div className="uk-h4">Story</div>
                    <div id="story" />

                    <div className="uk-h4">Posts</div>
                    <CommentsContainer {...fundraiser} />
                    <div className="uk-h4">Donations</div>
                    <DonationContainer {...fundraiser} />
                    <NavLink style={{ 'color': 'black' }} to='/donations' activeClassName="uk-active">
                        <button className="uk-button uk-button-primary" id="donate">Donate Now</button>
                    </NavLink>
                </div>
            </div>
        );
    }
}

export default FundraiserDetailPage;