import React, { Component } from 'react';
import axios from 'axios';
import Quill from 'quill';

let quill;
class AddFundraiserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            story: ''
        }
    }

    componentDidMount() {
        let toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],
            [{ 'header': 1 }, { 'header': 2 }],
            [{ 'list': 'ordered' }, { 'list': 'bullet' }],
            [{ 'script': 'sub' }, { 'script': 'super' }],
            [{ 'indent': '-1' }, { 'indent': '+1' }],
            [{ 'direction': 'rtl' }],
            [{ 'size': ['small', false, 'large', 'huge'] }],
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            [{ 'color': [] }, { 'background': [] }],
            [{ 'font': [] }],
            [{ 'align': [] }],
            ['link', 'image', 'video'],
            ['clean']
        ];

        quill = new Quill('#story', {
            modules: {
                toolbar: toolbarOptions
            },
            placeholder: 'Write your story...',
            theme: 'snow'
        });
    }


    handleSubmit = (e) => {
        e.preventDefault();
        let story = quill.getContents();
        this.setState({
            story: JSON.stringify(story)
        }, function () {
            let data = this.state;
            axios({
                method: 'post',
                url: '/api/v1/fundraisers',
                data: data
            })
                .then((petitions) => {
                    console.log('Fundraiser Added Successfully');
                });
        });
    }

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div className="uk-section">
                <div className="uk-container uk-container-small">
                    <form onSubmit={this.handleSubmit}>
                        <fieldset className="uk-fieldset">
                            <legend className="uk-legend">Legend</legend>
                            <div className="uk-margin">
                                <input id="title" name="title" className="uk-input" type="text" placeholder="Title" onChange={this.handleInputChange} />
                            </div>
                            <div className="uk-margin">
                                <textarea id="description" name="description" className="uk-textarea" rows="5" placeholder="Description" onChange={this.handleInputChange}></textarea>
                            </div>
                            <div className="uk-margin">
                                <input id="user_id" name="user_id" className="uk-input" type="text" placeholder="User Id" onChange={this.handleInputChange} />
                            </div>
                            <div className="uk-margin">
                                <div id="story" name="story" className="uk-height-large" />
                            </div>
                            <button className="uk-button uk-button-primary">Submit</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        );
    }
}

export default AddFundraiserPage;