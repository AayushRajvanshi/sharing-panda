import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import FundraiserCard from '../../components/Fundraiser/FundraiserCard';

class BrowseFundraiserPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fundraisers: []
    }
  }

  componentDidMount() {
    axios({
      method: 'get',
      url: '/api/v1/fundraisers'
    })
      .then((fundraisers) => {
        this.setState({
          fundraisers: fundraisers.data.docs
        })
      });
  }
  render() {
    let { fundraisers } = this.state;
    return (
      <div className="uk-section">
        <div className="uk-container uk-container-small">
          <div className="uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-small uk-grid-match uk-flex" data-uk-grid>
            {fundraisers.map((fundraiser) => {
              return <div key={fundraiser._id}>
                <FundraiserCard id={fundraiser._id} {...fundraiser}></FundraiserCard>
              </div>;
            })
            }
          </div>
        </div>
      </div>
    );
  }
}

export default BrowseFundraiserPage;