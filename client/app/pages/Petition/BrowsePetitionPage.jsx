import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import PetitionCard from '../../components/Petition/PetitionCard';

class BrowsePetitionPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            petitions: []
        }
    }

    componentDidMount() {
        axios({
            method: 'get',
            url: '/api/v1/petitions'
        })
            .then((petitions) => {
                this.setState({
                    petitions: petitions.data.docs
                })
            });
    }
    render() {
        let { petitions } = this.state;
        return (
            <div className="uk-section">
                <div className="uk-container uk-container-small">
                    <div className="uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-small uk-grid-match uk-flex" data-uk-grid>
                        {petitions.map((petition) => {
                            return <div key={petition._id}>
                                <PetitionCard id={petition._id} {...petition}></PetitionCard>
                            </div>;
                        })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default BrowsePetitionPage;